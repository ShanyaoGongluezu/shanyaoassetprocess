#!/usr/bin/python
with open("appveyor_config.json", 'r') as f:
    import json
    appveyor_config = json.loads(f.read())

import sys
luade_dir = "./luade"
luadecomp_dir = "./luadecomp"
commit_msg = sys.argv[1]
ori_stdout = sys.stdout
ori_stderr = sys.stderr
sys.stderr = sys.stdout = open(luade_dir + '/logs.txt', 'w')

import os
import subprocess

curdir = os.path.dirname(os.path.realpath(__file__)) # to parse the symlink to the real repo

DECRYPTER = os.path.join(curdir, "decrypter/decrypt.py")
DECOMPILER = os.path.join(curdir, "decompiler/main.py")

subprocess.call(["python", DECRYPTER, "full/lua", "luade/lua"], stdout=sys.stdout, stderr=sys.stderr)
subprocess.call(["python", DECRYPTER, "full/lua64", "luade/lua64"], stdout=sys.stdout, stderr=sys.stderr)

sys.stdout.close()
sys.stdout = ori_stdout
sys.stderr = ori_stderr

subprocess.check_output("git add .", shell=True, cwd=os.path.abspath(luade_dir))
subprocess.check_output(('git commit' + ' -m "%s" -a') % (commit_msg), shell=True, cwd=os.path.abspath(luade_dir))
subprocess.call('git push --force', shell=True, cwd=os.path.abspath(luade_dir))

def callAppveyorBuild():
    import requests
    r = requests.post("https://ci.appveyor.com/api/builds",
          headers={"Authorization": "Bearer %s" % appveyor_config["token"], "Content-type": "application/json"},
          json={
              "accountname": appveyor_config["accountName"],
              "projectslug": appveyor_config["projectSlug"],
              "branch": 'master',
          }
    )
    print(r.text)

callAppveyorBuild()