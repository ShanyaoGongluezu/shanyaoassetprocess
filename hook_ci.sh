#!/usr/bin/python2

import sys
luade_dir = "./luade"
luadecomp_dir = "./luadecomp"
commit_msg = sys.argv[1]
ori_stdout = sys.stdout
ori_stderr = sys.stderr
sys.stderr = sys.stdout = open(luadecomp_dir + '/logs.txt', 'w')

import os
import subprocess

curdir = os.path.dirname(os.path.realpath(__file__)) # to parse the symlink to the real repo

DECRYPTER = os.path.join(curdir, "decrypter/decrypt.py")
DECOMPILER = os.path.join(curdir, "decompiler/main.py")

FNULL = open(os.devnull, 'w')
def decompileFile(infn, outfn):
    print "Calling: ", ["python3", DECOMPILER, "-f", infn, "-c", "-o", outfn]
    subprocess.call(["python3", DECOMPILER, "-f", infn, "-c", "-o", outfn], stdout=FNULL, stderr=FNULL)
    #subprocess.call(["python3", DECOMPILER, "-f", infn, "-c", "-o", outfn])

def decompile(indir, outdir):
    for root, dirs, files in os.walk(indir, topdown=True):
        for dir in dirs:
            outd = os.path.join(os.path.join(outdir, os.path.relpath(root, indir)), dir)
            print "Making %s" % outd
            try:
                os.makedirs(outd)
            except:
                pass
        
        for file in files:
            if file.endswith(".lua"):
                infn = os.path.join(root, file)
                outfn = os.path.join(os.path.join(outdir, os.path.relpath(root, indir)), file)
                print "Decompiling %s" % (infn)
                decompileFile(infn,outfn)

decompile("luade/lua64", "luadecomp/lua64")
decompile("luade/lua", "luadecomp/lua")

sys.stdout.close()
sys.stdout = ori_stdout
sys.stderr = ori_stderr

print(subprocess.check_output("git status", shell=True, cwd=os.path.abspath(luadecomp_dir)))
print(subprocess.check_output("git add .", shell=True, cwd=os.path.abspath(luadecomp_dir)))
print(subprocess.check_output(('git commit' + ' -m "%s" -a') % (commit_msg), shell=True, cwd=os.path.abspath(luadecomp_dir)))
subprocess.call('git push --force', shell=True, cwd=os.path.abspath(luadecomp_dir))